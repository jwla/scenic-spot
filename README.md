# 智慧景区管理系统

#### 介绍
智慧景区管理系统，基于Java + SpringBoot + Mybatis Plus + Redis + Vue + antdv，支持景区管理、售票、地块管理、认养、商城、农资管理、积分兑换等功能，包含小程序，系统管理后台。

#### 软件架构
支持多租户切换等

#### 快捷体验chatGPT

<img src="https://foruda.gitee.com/images/1680944367241607254/eb18ac41_2042292.jpeg" width="200px" />

#### 演示地址

如需体验小程序和管理系统, 请添加下方微信

#### 联系方式
![输入图片说明](https://images.gitee.com/uploads/images/2022/0714/184028_03f76501_2042292.jpeg "28111657795185_.pic.jpg")

#### 演示图

![输入图片说明](https://foruda.gitee.com/images/1669796257290418656/4d8b850c_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1669796328537387508/5bd581bd_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1669796395215998442/ed49fb25_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1669796464417206314/bd38d836_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1669796963964181229/e1a3ac61_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1669797013285870218/9f714821_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1669797382299905860/6fb4d4d1_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1669798328169498361/5db791af_2042292.png)
![输入图片说明](https://foruda.gitee.com/images/1669797421245021382/048850e6_2042292.png)

